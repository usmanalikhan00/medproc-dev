var controller = require('./vendorcontract.ctrl.js')

module.exports = function(router){
  router.get('/allvendorcontracts', controller.allVendorContracts);
  router.post('/addvendorcontract', controller.addVendorContract);
}