var controller = require('./demand.ctrl.js')

module.exports = function(router){
    router.get('/alldemands', controller.alldemands )
    router.get('/demand', controller.demand )
    router.post('/editdemand', controller.editdemand )
    router.post('/updatedemandstate', controller.updatedemandstate )
    router.post('/updatetenderdemandstate', controller.updatetenderdemandstate )
    router.post('/updatedeptdemandstate', controller.updatedeptdemandstate )
    router.post('/updatedistrictdemandstate', controller.updatedistrictdemandstate )
    router.get('/departmentdemands', controller.departmentdemands )
    router.get('/tenderdemands', controller.tenderdemands )
    router.get('/dddemands', controller.dddemands )
    router.post('/adddemand', controller.adddemand )
}