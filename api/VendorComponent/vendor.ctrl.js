const VendorModel = model('vendor');
const UserModel = model('user');
exports.allvendors = function(req,res,next){
    // next(); IF ERROR OCCURS
    //res.send("Get all vendors");
    // UserModel.find().
    //     where('_id').equals(req.params.userId).
    //     exec(function(err, users){
    //         if(err){
    //             res.send(err);
    //         }
    //         res.json(users);
    // });
    UserModel.findById(req.query.userId, function (err, user) {
        if (err) {
            res.json(err);
        }
        if(user.role==='admin' || user.role==='user' || user.role==='pco'){
            VendorModel.find(function(err, vendors){
                if (err){
                    res.json(err);
                } 
                res.json([{status:1,message:'Access Granted',vendors:vendors}]);
            });
        }
        else {
            res.json([{status:0,message:'Access Denied',vendors:[]}]);
        }
    }) 
}
exports.addvendor = function(req,res,next){

    var saveVendor = model('vendor'); 
    var newVendor = new saveVendor({
                                    name: req.body.name,
                                    address: req.body.address,
                                    phonenumber: req.body.phonenumber,
                                    city: req.body.city,
                                    email: req.body.email,
                                    website: req.body.website,
                                    dateofestablishment: req.body.dateofestablishment,
                                    focalpersonname: req.body.focalpersonname,
                                    focalpersonnumber: req.body.focalpersonnumber,
                                    businesstype: req.body.businesstype,
                                });
    UserModel.findById(req.body.userId, function (err, user) {
        if (err) {
            res.json(err);
        }
        if(user.role==='admin'){
            newVendor.save(function(err, result){
                if(err){ res.json(err); }
                res.json([{status:1,message:'Access Granted',result:result}]);
            });
        }
        else {
            res.json([{status:0,message:'Access Denied',result:[]}]);
        }
    }) 
}