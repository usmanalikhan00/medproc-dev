import {Component, ElementRef, ViewChild} from '@angular/core';
import {LoginService} from '../../../services/login.services'
import {TenderService} from '../../../services/tender.service'
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import * as moment from "moment";
import { GlobalEventsManager } from "../../../services/eventsmanager.service";

@Component({
    moduleId: module.id,
    selector: 'nav-bar',
    providers: [TenderService],
    templateUrl: 'nav.html',
    styleUrls: ['nav.css']
})

export class nav {
  
  showNavBar: boolean = false;
  isMobileMenu: boolean = true;

  userRole = localStorage.getItem('userrole');
  constructor(private _loginService: LoginService,
              private _eventMangaerService: GlobalEventsManager){
    this._eventMangaerService.showNavBarEmitter.subscribe((mode)=>{
      // mode will be null the first time it is created, so you need to igonore it when null
      if (mode !== null) {
        this.showNavBar = mode;
      }
    });

    this.isNotMobileMenu();
  }

  logout(){
    // this._eventMangaerService.showNavBar(false);
    window.location.reload();
    this._loginService.logout();
  }
  isNotMobileMenu(){
      if((window.screen.width) > 991){
        this.isMobileMenu = false;
          return this.isMobileMenu;
      }
        this.isMobileMenu = true;
      return this.isMobileMenu;
  }

}