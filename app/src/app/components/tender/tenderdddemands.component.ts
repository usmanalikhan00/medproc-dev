import { Component, ElementRef } from '@angular/core';
import { Location } from '@angular/common';
import { LoginService } from '../../services/login.services'
import { DemandService } from '../../services/demand.service'
import { ConfigsService } from '../../services/configs.service'
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import * as moment from "moment";
import { GlobalEventsManager } from "../../services/eventsmanager.service";
import 'rxjs/add/operator/switchMap';

@Component({
    selector: 'tenderdddemands',
    providers: [DemandService,  
                ConfigsService ],
    templateUrl: 'tenderdddemands.html',
    styleUrls: ['tender.css']
})

export class tenderdddemands {
  demands = []
  tenderName = ""
  departmentName = ""
  districtName = ""
  medPrice = []
  demStatus = []

  constructor(private _router: Router,
              private _route: ActivatedRoute, 
              private _location: Location, 
              private _demandService: DemandService,
              private _loginService: LoginService,
              private _configsService: ConfigsService,
              private _eventMangaerService: GlobalEventsManager){

    this._eventMangaerService.showNavBar(true);
  }
  acceptOrRejectCall(i){
    let demState = this.demStatus[i]
    this._route.paramMap
      .switchMap((params: ParamMap) =>
        this._demandService.updateDemandStatus(this.demands[i]._id,demState))
      .subscribe(demands =>{
        console.log("Dem Status Resp",demands)
      });
  }
  acceptOrReject(dep,i){
    if (!dep) {
      this.demStatus[i] = 1
      this.acceptOrRejectCall(i);
    } else  {
      this.demStatus[i] = 0
      this.acceptOrRejectCall(i);
    }
    console.log("Checkbox->",this.demStatus);
  }
  setDemStatus(){
    for (let i=0;i<this.demands.length;i++){
      this.demStatus[i] = 0;
    }
  }
  setMedPrice(){
    for (let i=0;i<this.demands.length;i++){
      this.medPrice[i] = '0.00'
      for (let j=0;j<this.demands[i].medicine.length;j++){
        this.medPrice[i] = parseFloat(this.demands[i].medicine[j].estprice) 
                          + parseFloat(this.medPrice[i]); 
      }
    }
  }
  ngOnInit() {
    this._route.paramMap
      .switchMap((params: ParamMap) =>
        this._demandService.getDDDemands(params.get('departmentid'),params.get('tenderid'),params.get('district')))
      .subscribe(demands => {
        if (!demands[0].status){

        } else {
          this.demands = demands[0].demands;
          this.tenderName = this.demands[0].tendername
          this.departmentName = this.demands[0].departmentname
          this.districtName = this.demands[0].district
          this.setMedPrice();
          this.setCheckboxes();
          console.log("Demands",this.demands);
        }
      });
    
  }
  setCheckboxes(){
    for (let j=0;j<this.demands.length;j++){
      if (this.demands[j].demandstatus === "1"){
        this.demStatus[j] = 1     
      }
    }
  }
  goBack(){
    this._location.back()
  }
  // ********** Routing Fucntions **********//
  logout(){
      this._loginService.logout();
  }
  goToRoute(name){
    this._router.navigate([name]);
  }

}