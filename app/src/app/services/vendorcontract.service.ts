import {Injectable} from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';
import { Router } from '@angular/router'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

var userId = localStorage.getItem("userid");

@Injectable()
export class VendorContractService{
  constructor(private http: Http, 
              private _router: Router){
    console.log('Vendor Contract Service Initialized...');
  }
  getAllVendorContracts(){
    return this.http.get("http://localhost:8008/api/allvendorcontracts",
                {params:{userId:userId}})
                .map(res => res.json());
  }  
  addVendorContract(vendorId,
                   tenderId, 
                   contractprofileId,
                   contractprofiletext,
                   contractdate){
    return this.http.post("http://localhost:8008/api/addvendorcontract",
                {userId:userId, 
                         vendorId:vendorId, 
                         tenderId:tenderId,
                         contractprofileId:contractprofileId,
                         contractprofiletext:contractprofiletext,
                         contractdate: contractdate})
                .map(res => res.json());
  }
}