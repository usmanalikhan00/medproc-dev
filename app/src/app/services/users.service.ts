import {Injectable} from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';
import { Router } from '@angular/router'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';


@Injectable()
export class UsersService{

    userId: any;
    constructor(private http: Http, private _router: Router){
        console.log('User Service Initialized...');
        this.userId = localStorage.getItem("userid");
    }
    getAllUsers(){
      console.log('GET ALL USERS CALLED');
      return this.http.get('http://localhost:8008/api/allusers',
                  {params:{userId:this.userId}})
                  .map(res => res.json());
    }
    addUser(newUser){
      console.log('Add USER CALLED');
      console.log('User To add: ', newUser.email);
      let headers = new Headers({ 'Content-Type': 'application/json' });
      let options = new RequestOptions({ headers: headers });
      return this.http.post('http://localhost:8008/api/adduser', {
                  "userId":this.userId,
                  "name":newUser.name,
                  "email":newUser.email,
                  "password":newUser.password,
                  "username":newUser.username,
                  "district":newUser.district,
                  "department":newUser.department,
                  "tehsil":newUser.tehsil,
                  "role":newUser.role,
                  "designation":newUser.designation}, options)
                  .map(res => res.json());

    }
}