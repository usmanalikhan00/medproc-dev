
const env = 'dev';

var express         = require('express');
var app             = express();
var bodyParser      = require('body-parser');
var path            = require('path');
var fs 				= require('fs');
var mongoose        = require('mongoose');
var _overides       = require(path.join(__dirname,'lib','cupcake.js'))();

var routes          = require(path.join(__dirname,'routes'));
var config          = require(path.join(__dirname,'config'))[env];
var dbInit          = require(path.join(__dirname,'dbConnection'))(config);

var port 			= config.port;                     // set our por
var router 			= express.Router();
// get an instance of the express Router

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

routes(router);

app.use('/uploads', express.static(path.join(__dirname, 'uploads')))
app.use('/api', router);

// IF ALL FAILS TURN TO THY LORD AND WISH FOR A MIRACLE, MEANWHILE OPEN THE APP BY DEFAULT \m/
var __staticPath = path.join(__dirname,'app','dist'); // dev path; use ng build --output-path=[folderName]/
// console.log(path.join(__dirname));
app.use('/' , express.static(__staticPath));
app.use('/*', express.static(__staticPath));

app.listen(port);

console.log('\n===================================');
console.log('Angular 2 Application hosted on:  ' + port);
console.log('       RESTful API Endpoints on:  ' + port + ' /api');
console.log('===================================\n');
