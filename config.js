module.exports = 
{
    "dev": {
        env: 'dev',
        port: 8008,
        appName: 'eprocurement',
        secret: 'devontherocks',
        dbString: 'mongodb://localhost:27017/eProcurement_dev',
    },
    "qa": {
        env: 'qa',
        port: 8009,
        appName: 'eprocurement',
        secret: 'devontherocks',
        dbString: 'mongodb://138.197.17.216:31426/eProcurement_qa',
    },
    "prd": {
        env: 'prd',
        port: 80,
        appName: 'eprocurement',
        secret: '0.515036214X',
        dbString: 'mongodb://138.197.17.216:31415/eProcurement',
    }
}